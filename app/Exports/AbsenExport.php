<?php

namespace App\Exports;

use Carbon\Carbon;

use App\Models\Presence;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;


class AbsenExport implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $startDate = request()->input('startDate') ;
        $endDate   = request()->input('endDate') ;
        return Presence::select('presences.*', 'attendances.title','users.name')
        ->leftJoin('attendances', 'presences.attendance_id', '=', 'attendances.id')
        ->leftJoin('users', 'presences.user_id', '=', 'users.id')
        ->orderBy('presences.user_id','ASC')
        ->whereBetween('presences.presence_date', [ $startDate, $endDate ] )
        ->get();
    }

    // public function map($presence) : array {
    //     return [
    //         // $presence->id,
    //         $presence->name,
    //         $presence->presence_date,
    //         $presence->presence_enter_time,
    //         $presence->presence_out_time,
    //         $presence->lokasi_masuk,
    //         $presence->lokasi_pulang,
    //         $presence->catatan_masuk === NULL ? '-' : $presence->catatan_masuk,
    //         $presence->catatan_pulang === NULL ? '-' : $presence->catatan_pulang,
    //         $presence->radius_masuk,
    //         $presence->radius_pulang,
    //         $presence->radius_pulang,

    //         // Carbon::parse($presence->presence_date)->toFormattedDateString()
    //     ] ;
    // }

    public function map($presence) : array {
        $enterTime = $presence->presence_enter_time;
        $statusEnterTime = 0; // Default status

        if ($enterTime <= '09:00:59') {
            $statusEnterTime = 0;
        } elseif ($enterTime >= '09:01:00' && $enterTime <= '09:30:59') {
            $statusEnterTime = 0.5;
        } elseif ($enterTime >= '09:31:00' && $enterTime <= '10:00:59') {
            $statusEnterTime = 1;
        }
         elseif ($enterTime >= '10:01:00' && $enterTime <= '10:30:59') {
        $statusEnterTime = 1.5;
        } elseif ($enterTime >= '10:31:00') {
            $statusEnterTime = 2;
        } else {
            $statusEnterTime = 2;
        }


        $outTime = $presence->presence_out_time;
        $statusOutTime = 0; // Default status

        if ($outTime >= '15:00:00') {
            $statusOutTime = 0;
        } elseif ($outTime >= '14:30:00' && $outTime <= '14:59:59') {
            $statusOutTime = 0.5;
        } elseif ($outTime >= '14:00:00' && $outTime <= '14:29:59') {
            $statusOutTime = 1;
        }
         elseif ($outTime >= '13:30:00' && $outTime <= '13:59:59') {
        $statusOutTime = 1.5;
        } elseif ($outTime <= '13:29:59') {
            $statusOutTime = 2;
        }
        // } else {
        //     $statusOutTime = 2;
        // }

        $ijinStatus = $presence->is_permission;
        $statusIjin = 0; // Default status
        if ($ijinStatus === 0) {
            $statusIjin = '-';
        } elseif ($ijinStatus === 1) {
            $statusIjin = 'Ijin';
        }


        return [
            // $presence->id,
            $presence->name,
            // $presence->presence_date,
            Carbon::parse($presence->presence_date)->isoFormat('dddd, D MMMM Y'),
            $presence->presence_enter_time,
            $presence->presence_out_time,
            $presence->lokasi_masuk,
            $presence->lokasi_pulang,
            $presence->catatan_masuk === NULL ? '-' : $presence->catatan_masuk,
            $presence->catatan_pulang === NULL ? '-' : $presence->catatan_pulang,
            $presence->radius_masuk,
            $presence->radius_pulang,
            $statusIjin,
            $statusEnterTime,
            $statusOutTime,
            $statusEnterTime + $statusOutTime,
        ];
    }

    public function headings() : array {
        return [
            // '#',
            'Nama',
            'Tanggal',
            'Jam Masuk',
            'Jam Pulang',
            'Lokasi Masuk',
            'Lokasi Pulang',
            'Catatan Masuk',
            'Catatan Pulang',
            'Status Radius Masuk',
            'Status Radius Pulang',
            'Status Ijin',
            'Potongan Telat Masuk (%)',
            'Potongan Cepat Pulang (%)',
            'Total Potongan (%)'
        ] ;
    }
}
