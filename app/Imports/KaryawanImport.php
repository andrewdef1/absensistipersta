<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Carbon\Carbon;


class KaryawanImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */


    public function model(array $row)
    {
        return new User([
                'name'     => $row[0],
                'email'    => $row[1],
                'password'    => $row[2],
                'phone'    => $row[3],
                'position_id'    => $row[4],
                'role_id'    => $row[5],
                // 'holiday_date'    => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[0])->format('Y-m-d'),
             ]);

    }
}