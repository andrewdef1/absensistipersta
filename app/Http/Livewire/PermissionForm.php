<?php

namespace App\Http\Livewire;

use App\Models\Permission;
use Livewire\Component;
use Livewire\WithFileUploads;

class PermissionForm extends Component
{
    use WithFileUploads;
    public $permission;
    public $attendanceId;
    public $file; // Add this property


    protected $rules = [
        'permission.title' => 'required|string|min:6',
        'permission.description' => 'required|string|max:500',
        'file' => 'nullable|mimes:pdf,doc,docx,jpg,jpeg,png,xls,xlsx|max:5048', // Add appropriate validation rules
    ];


    public function save()
    {
        $this->validate();

        $permission = Permission::create([
            "user_id" => auth()->user()->id,
            "attendance_id" => $this->attendanceId,
            "title" => $this->permission['title'],
            "description" => $this->permission['description'],
            "permission_date" => now()->toDateString()
        ]);

        // menyimpan data file yang diupload ke variabel $file
        if($this->file){

        $nama_file = time() . '_' . $this->file->getClientOriginalName();
         // isi dengan nama folder tempat kemana file diupload
    //$fupload->move($tujuan_upload,$nama_file);
if($this->file){
    $this->file->storeAs('public/uploads', $nama_file);
    $permission->update(['fileup' => $nama_file]);
}
}

        // if ($this->file) {
        //     $fileName = time() . '_' . $this->file->getClientOriginalName();
        //     $this->file->storeAs('public/uploads', $fileName);

        //     $permission->update(['fileup' => $fileName]);
        // }

        return redirect()->route('home.show', $this->attendanceId)->with('success', 'Permintaan izin sedang diproses. Silahkan tunggu...');
    }

    public function render()
    {
        return view('livewire.permission-form');
    }
}