@extends('layouts.base')
<link rel="shortcut icon" href="{{ URL::asset('/img/logostiper.ico') }}" />
<link rel="icon" href="{{ URL::asset('/img/logostiper.ico') }}" type="image/x-icon"/>
    @if(session('status'))
    <div class="alert alert-success alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      {{session('status')}}
    </div>
    @endif
    {{-- <div class="row" style="margin: auto;">
        </div> --}}
    <div class="row pt-3 pl-3 pr-3">
      <div class="card" style="width: 100%">
        <div class="card-header">
          <h4 class="inline-block pt-2">CETAK LAPORAN ABSEN EXCEL</h4>

        </div>
        <div class="card-body">
            <form method="get" action="{{ route('laporan.cetakexcel') }}">
                <div class="form-group">
                    <label>Tanggal Awal:</label>
                    <div class="input-group mb-3">
                        <input  class="form-control" type="date" id="startDate" name="startDate" width="276" />
                    </div>
                </div>

                <div class="form-group">
                    <label>Tanggal Akhir:</label>
                    <div class="input-group mb-3">
                        <input  class="form-control" type="date" id="endDate"  name="endDate" width="276" />
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group mb-3">
                        <button type="submit" class="btn btn-primary">Export to excel</button>
                    </div>
                </div>

            </form>
        </div>
      </div>
    </div>

