@extends('layouts.home')

@section('content')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css">
{{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> --}}



@if(session('status'))
<div class="alert alert-success alert-dismissible">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  {{session('status')}}
</div>
@endif

<div class="card mb-3">
    <div class="card-body">
        <form enctype="multipart/form-data" action="{{route('profile.update', [$users->id])}}" method="POST">
            @csrf
            <input type="hidden" value="PUT" name="_method">
            <label for="email">Email</label>
            <input readonly value="{{old('email') ? old('email') : $users->email}}" class="form-control {{$errors->first('email') ? "is-invalid" : ""}}" placeholder="Email" type="text" name="email" id="email" />
            <div class="invalid-feedback">
              {{$errors->first('email')}}
            </div>
            <br>

            <label for="name">Nama</label>
            <input value="{{old('name') ? old('name') : $users->name}}" class="form-control {{$errors->first('name') ? "is-invalid" : ""}}" placeholder="Nama" type="text" name="name" id="name" />
            <div class="invalid-feedback">
              {{$errors->first('name')}}
            </div>
            <br>


            <label for="phone">Phone</label>
            <input value="{{$users->phone}}" class="form-control {{$errors->first('phone') ? "is-invalid" : ""}}"
            placeholder="Nomor HP" type="number" name="phone" id="phone" />
            <div class="invalid-feedback">
              {{$errors->first('phone')}}
            </div>
            <br>

            <label for="password">Password</label>
            <input  class="form-control {{$errors->first('password') ? "is-invalid" : ""}}"
            placeholder="Password Baru" type="password" name="password" id="password" />
            <div class="invalid-feedback">
              {{$errors->first('password')}}
            </div>
            <small class="text-muted">*Kosongkan jika tidak ingin mengubah PASSWORD</small>
            <br>


            <br>
            <input class="btn btn-primary" type="submit" value="Save" />
          </form>
</div>


@endsection
